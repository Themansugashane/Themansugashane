# Shane Brooks


## Software Developer: My goal is to learn full stack development and data science this year.

Once I graduate college I want to get my masters degree in Data Science and become a machine learning engineer. I see Data Science & Machine Learning as the future of technology especially when it comes to robotics. I am hoping to gain a lot of knowledge in this course and apply it in my everyday life.

## Hobbies and Interests
I love to workout and learn about the stock market. Financial literacy is super important to me because it gives people a better understanding of their everyday finaces. Impacting peoples lives in a positive way is one of my biggest goals in life. If I can inspire one  soul per day than I did my job.

## Experiences 

Languages: C++ , HTML, CSS , and some python

School Organizations: Morehouse Divison II Football Team , Residential Advisor, Mcnair Scholar , Student Ambassandor , International Chair for the National Society of Black Engineers , Tresurer for TRIBE

